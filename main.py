#!/usr/bin/env python3

# Created On: 2018 October 11
# Created By: Pete Maynard

# Description: Run a web service.

import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from flask import Flask 
from flask import request
from flask import render_template
from flask import make_response
from flask import url_for

from flask import send_from_directory # TODO: nginx: Static serve.

from werkzeug.contrib.cache import FileSystemCache

ENVIRONMENT = os.environ.get("FLASK_ENV")

if ENVIRONMENT == 'production' or not ENVIRONMENT : 
	print(" * Sentry Monitoring Enabled")
	import sentry_sdk
	from sentry_sdk.integrations.flask import FlaskIntegration

	sentry_sdk.init(
	    dsn="https://88966d19d83a473d806ba322298640be@sentry.io/1300043",
	    integrations=[FlaskIntegration()]
	)

from tables import *

#
# Database Stuffs 
#

engine = create_engine('sqlite:///data/database.db', echo=False)
Session = sessionmaker(bind=engine)

#
# Web Stuffs
#

app = Flask(__name__, static_url_path='')
cache = FileSystemCache("data/cache")


@app.route('/', methods=['GET'])
@app.route('/about', methods=['GET'])
def about():
	return render_template("about.html")

@app.route('/news', methods=['GET'])
def news():
	return render_template("news.html")

@app.route('/search', methods=['GET', 'POST'])
def search_pkg():
	if request.method == 'POST' and len(request.form['s']) > 2:
		session = Session()
		return render_template("search.html", results=session.query(Ware).filter(Ware.name.like("%"+request.form['s'].strip()+"%")).all())
	else:
		return render_template("search.html")

@app.route('/categories', methods=['GET'])
def list_cat():
	session = Session()
	res = cache.get('categories_full')
	if res is None:
		res = session.query(Category).group_by(Category.name).all()
		cache.set('categories_full', res)
		print("| Setting cache")
	return render_template("categories.html", results=res)

@app.route('/category/<string:cat>', methods=['GET'])
def get_cat(cat):
	session = Session()

	# TODO: Fix this hack.
	if len(session.query(Category).filter(Category.name == cat).all()) == 0 :
		return render_template('page_not_found.html', msg="The category you are looking for is not in our database."), 404
	else:
		res = cache.get('category_'+cat)
		if res is None:
			res = session.query(Ware).filter(Ware.categories.any(name=cat)).all()
			cache.set('category_'+cat, res)
			print("| Setting cache")
		return render_template("search.html", results=res)

@app.route('/pkg/<string:pkg>', methods=['GET'])
def get_pkg(pkg):
	session = Session()
	pk = session.query(Ware).filter(Ware.name == pkg).one_or_none()

	if pk is None:
		return render_template('page_not_found.html', msg="The package you are looking for is not in our database."), 404
	else:
		return render_template("singlepkg.html", p=pk)

@app.errorhandler(404)
def page_not_found(error):
	return render_template('page_not_found.html'), 404

# TODO: NGINX 
@app.route('/assets/<path:path>')
def send_js(path):
	return send_from_directory('static/assets/', path)

@app.route('/images/<path:path>')
def send_img(path):
	return send_from_directory('static/images/', path)
