
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column, Integer, String, Text
from sqlalchemy import ForeignKey

Base = declarative_base()

ware2cat = Table('ware2cat', Base.metadata, Column('ware_id', ForeignKey('ware.id'), primary_key=True), Column('cat_id', ForeignKey('category.id'), primary_key=True))


class Ware(Base):
	__tablename__ = 'ware'

	id = Column(Integer, primary_key=True)

	name = Column(String, unique=True)
	origin = Column(String)
	version = Column(String)
	comment = Column(String)
	maintainer = Column(String)
	www = Column(String)
	abi = Column(String)
	arch = Column(String)
	prefix = Column(String)
	sumhash = Column(String)
	pkgsize = Column(String)
	flatsize = Column(String)
	path = Column(String)
	repopath = Column(String)
	licenselogic = Column(String)
	pkgsize = Column(String)
	desc = Column(String)
	www2 = Column(String)
	deps = Column(String)
	annotations = Column(String)

	categories = relationship('Category', secondary=ware2cat, back_populates='ware')

class Category(Base):
	__tablename__ = 'category'
	
	id = Column(Integer, primary_key=True)
	name = Column(String, nullable=False)
	ware = relationship('Ware', secondary=ware2cat, back_populates='categories')


# class Dep(Base):
# 	__tablename__ = 'dep'
	
# 	id = Column(Integer, primary_key=True)
# 	ware_id 

# class DepTWare(Base)

# class License(Base):
# 	__tablename__ = 'license'
	
# 	id = Column(Integer, primary_key=True)
# 	name = Column(String, unique=True)
