#!/usr/bin/env python3

# Created On: 2018 October 11
# Created By: Pete Maynard

# Description: Parse the ports yaml file and save into an SQLlite DB

import sys
import argparse
import yaml

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from tables import *

# Handle commandline arguments.
parser = argparse.ArgumentParser(description=' Parse the ports yaml file and save into an SQLlite DB.')
parser.add_argument('-f', '--file', help="Location of the ports yaml file.", default='data/packagesite.yaml')
parser.add_argument('--db', help="Location of the SQLite database.", default='data/database.db')
args = parser.parse_args()

# TODO: Check locations etc.

# Database 

engine = create_engine('sqlite:///'+args.db, echo=False)
Session = sessionmaker(bind=engine)

# Make sure all the tables exist.
Base.metadata.create_all(engine)

i = 1
lineNo = 1

with open(args.file, 'r', encoding='utf-8', errors='replace') as file:
	session = Session()
	for line in file:
		pkg = yaml.safe_load(line)
		print(lineNo)
		lineNo = lineNo + 1

		ware = Ware(
				name=pkg['name'],
				origin=pkg['origin'],
				version=pkg['version'],
				comment=pkg['comment'],
				maintainer=pkg['maintainer'],
				www=pkg['www'],
				abi=pkg['abi'],
				arch=pkg['arch'],
				prefix=pkg['prefix'],
				sumhash=pkg['sum'],
				flatsize=pkg['flatsize'],
				path=pkg['path'],
				repopath=pkg['repopath'],
				licenselogic=pkg['licenselogic'],
				pkgsize=pkg['pkgsize'],
				desc=pkg['desc']
				# www2=pkg['WWW'],
				# deps=pkg['deps'],
				# categories=pkg['categories']
				# annotations=pkg['annotations']
		)

		for n in pkg['categories']:
			# Check for new categories.
			# if not session.query(Category).filter(Category.name == n).one_or_none():
			# 	session.add(Category(name=n))
			# 	session.commit()
			# 	print("Added new Category:", n)

			# TODO: Fix the categories table from having duplicate entries.
			ware.categories.append(Category(name=n))

		session.add(ware)

		i = i +1
		if i == 1000:
			session.commit()
			i = 0
			print("1000 Commit")

session.commit()
