# Visual BSD

An attempt to create a visually, pretty index of packages for systems that use [pkg](https://github.com/freebsd/pkg).

Currently deployed at: <https://visbsd.port22.co.uk>

# Development

Configure a virtual environment

    python3 -m venv venv
    . venv/bin/activate

Install dependencies

    pip install Flask sqlalchemy pyyaml sentry-sdk[flask]==0.4.0

Run the service.

    export FLASK_ENV=development
    export FLASK_APP=main.py
    python -m flask run


